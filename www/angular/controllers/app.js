var app = angular.module('appModule', [
    'ngRoute', 'LocalStorageModule']);

app.config(function ($httpProvider, $routeProvider, $locationProvider) {

    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};

    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.cache = false;
    $locationProvider.hashPrefix('');

    $routeProvider.when('/logout',
        {
            templateUrl: 'partials/sign-up/logout.html',
            reloadOnSearch: false
        });
    $routeProvider.when('/login',
        {
            templateUrl: 'partials/sign-up/login.html',
            reloadOnSearch: false
        });
    $routeProvider.when('/register',
        {
            templateUrl: 'partials/sign-up/register.html',
            reloadOnSearch: false
        });
    $routeProvider.when('/home',
        {
            templateUrl: 'partials/home.html',
            reloadOnSearch: false
        });
    $routeProvider.when('/user-profile',
        {
            templateUrl: 'partials/user/update-profile.html',
            reloadOnSearch: false
        });
    $routeProvider.otherwise({
        templateUrl: 'partials/home.html'
    });

})

app.controller('MainController', function (
    $rootScope,
    $scope, $location,
    appService,
    localStorageService,
    HttpRequest) {

    $scope.user = {};
    $scope.user.currentMenu = 'account';

    $scope.user.updateCurrentMenu = function (menu) {

        $scope.user.currentMenu = menu;
        $scope.user.togglemenu();
    }


    $scope.user.showMenu = true;

    $scope.user.togglemenu = function () {
        if (!$scope.user.showMenu) {
            $('#sidebar').removeClass('active');
        } else {
            $('#sidebar').addClass('active');
        }
        $scope.user.showMenu = !$scope.user.showMenu;
    }
    $scope.user.hidemenu = function () {

        $('#sidebar').removeClass('active');

        $scope.user.showMenu = false;
    }


    $scope.user.userFormData = undefined

    /***
     * regualr expressions
     * */

    $scope.user.regex = {
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    }

    $scope.user.validateEmail = function (email) {
        console.log(email)
    };

    /***
     * branch array declaration
     * **/
    $scope.user.customerData = [];
    $scope.user.reloadForm = false;
    $scope.user.saveStatus = false;
    $scope.user.loggedInUserDetails = localStorageService.get('loggedInUserDetails');

    $rootScope.$on('$locationChangeStart', function (evt, absNewUrl, absOldUrl) {
        let loggedInUser = localStorageService.get('loggedInUserDetails');
        console.log(loggedInUser)
        if (loggedInUser) {
            if (absNewUrl.includes("/login")) {
                $location.path('/user-profile');
            }
        } else {
            if ($location.path() === '' || $location.path() === '/register') {

            } else {
                $location.path('/login');
            }
        }
    })
    $scope.user.saveStatus = false;
    $scope.user.errorMessage = undefined;
    $scope.user.signin = function (loginFormData) {
        loginFormData.url = LOGIN.Login;
        $scope.user.errorMessage = undefined;
        $scope.user.saveStatus = true;
        try {
            HttpRequest.MakePostHttp(loginFormData).then(function (response) {
                let res = response;
                $scope.user.saveStatus = false;
                if (res.status == 1) {
                    $scope.user.loginFormData = {};

                    localStorageService.set('loggedInUserDetails', res.data);
                    $scope.user.loggedInUserDetails = res.data;
                    $location.path('/user-profile');
                } else {
                    $scope.user.errorMessage = res.message;
                }
            })
        } catch (ex) {
            $scope.user.errorMessage = "A server error occured,please try again";
            $scope.user.saveStatus = false;
        }
    }
    $scope.user.updateUserProfileForm = function () {
        $scope.user.userFormData = {
            fullname: $scope.user.loggedInUserDetails.fullname,
            email: $scope.user.loggedInUserDetails.email,
        };
        $scope.user.showEditProfile = true;
    }
    $scope.user.showEditProfile = false;
    $scope.user.updateProfile = function (userFormData) {
        userFormData.url = USERS.UpdateProfile + "/" + $scope.user.loggedInUserDetails.id;
        $scope.user.errorMessage = undefined;
        $scope.user.saveStatus = true;
        $scope.user.showEditProfile = false;
        try {
            HttpRequest.MakePostHttp(userFormData).then(function (response) {
                let res = response;
                if (res.status == 1) {
                    $scope.user.saveStatus = false;
                    localStorageService.set('loggedInUserDetails', res.data);
                    $scope.user.loggedInUserDetails = res.data;
                } else {
                    $scope.user.errorMessage = res.message;
                }
            })
        } catch (ex) {
            $scope.user.errorMessage = "A server error occured,please try again";
            $scope.user.saveStatus = false;
        }
    }

    $scope.user.logout = function () {
        localStorageService.set('loggedInUserDetails', null);
        $scope.user.loggedInUserDetails = undefined;
        $location.path('/login');
    }
    $scope.user.successStatus = undefined;
    $scope.user.register = function (registerFormData) {
        registerFormData.url = LOGIN.Register;
        $scope.user.errorMessage = undefined;
        $scope.user.successStatus = undefined;
        $scope.user.saveStatus = true;
        HttpRequest.MakePostHttp(registerFormData).then(function (response) {
            let res = response;
            $scope.user.saveStatus = false;
            if (res.status == 1) {
                $scope.user.userFormData = {};

                $scope.user.successStatus = res.message;
            } else {
                $scope.user.errorMessage = res.message;
            }
        });
    }
    $scope.user.saveTempCustomer = function (customer) {
        localStorageService.set('customerDetails', customer);
    }


});

app.directive('mandatoryField', function () {
    return {
        template: "<b class='text-danger' > *</b> "
    };
});

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('APIInterceptor');
}]);
app.service('APIInterceptor', ['localStorageService',

    function (localStorageService) {
        let service = this;

        service.request = function (config) {
            config.headers.api_key = API_KEY;
            let logIn = localStorageService.get('userCookieDetails');
            if (logIn) {
                config.headers.API_TOKEN = logIn.AUTH_KEY;
            }

            return config;
        };
    }])

